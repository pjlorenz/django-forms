from django.core.exceptions import ValidationError
from django.utils.translation import gettext as _

def validate_zipcode(value):
    if len(value) != 5:
        raise ValidationError(_("Fields need to be of length 5: %(value)s"),
                              code='invalid_length',
                              params={'value': value},
                              )

