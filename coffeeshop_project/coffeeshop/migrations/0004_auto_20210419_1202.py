# Generated by Django 3.2 on 2021-04-19 16:02

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('coffeeshop', '0003_tbluserdetails'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='TblUser',
            new_name='User',
        ),
        migrations.RenameModel(
            old_name='TblUserDetails',
            new_name='UserDetails',
        ),
    ]
