from django.template import loader
from django.http import HttpResponse
from django.shortcuts import render, redirect
from .forms import NameForm, UserForm, UserDetailsForm
from django.http import HttpResponseRedirect
from django.contrib.auth import login
from django.forms.models import modelformset_factory
from django.forms.formsets import formset_factory

from .models import Coffee
from .utils import LARGE, AMERICANO, CAPPUCINO, COLD


def home(request):
    template = loader.get_template('home.html')
    context = {}
    return HttpResponse(template.render(context, request))


def thanks(request):
    template = loader.get_template('thanks.html')
    context = {}
    return HttpResponse(template.render(context, request))


def signup(request):
    if request.method == 'POST':
        form = UserForm(request.POST)
        if form.is_valid():
            user = form.save()
            login(request, user)
            return HttpResponseRedirect('/details/')

    else:
        form = UserForm(initial={'email': '@gmail.com'})
    return render(request, 'signup.html', {'form': form})


def details(request):
    if request.method == 'POST':
        form = UserDetailsForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/thanks/')
    else:
        form = UserDetailsForm(initial={'username': request.user.username, 'email': request.user.email})

    return render(request, 'signup.html', {'form': form})

def placeOrder(request):
    OrderFormSet = modelformset_factory(Coffee, fields=['name', 'size', 'quantity'], max_num=3, extra=3)
    if request.method == 'POST':
        order_formset = OrderFormSet(request.POST)
        if order_formset.is_valid():
            order_formset.save()
            return redirect('home')
    else:
        order_formset = OrderFormSet(initial=[{'name': AMERICANO, 'size': LARGE, 'quantity': '1'},
                                              {'name': CAPPUCINO, 'size': LARGE, 'quantity': '1'},
                                              {'name': COLD, 'size': LARGE, 'quantity': '1'}
                                            ],
                                     queryset=Coffee.objects.none())
    return render(request, 'order.html', {'formset': order_formset})
