from django.contrib import admin
from coffeeshop.models import UserDetails, Coffee


# Register your models here.

admin.site.register(UserDetails)
admin.site.register(Coffee)
