SMALL = 'S'
MEDIUM = 'M'
LARGE = 'L'
SIZE_CHOICES = [
    (None, 'Please select a size'),
    (SMALL, 'Small'),
    (MEDIUM, 'Medium'),
    (LARGE, 'Large'),
]

AMERICANO = 'A'
CAPPUCINO = 'C'
COLD = 'CB'

COFFEE_CHOICES = [
    (None, 'Please select a drink type'),
    (AMERICANO, 'Americano'),
    (CAPPUCINO, 'Cappucino'),
    (COLD, 'Cold')
]

QUANTITY_CHOICES = [('1', '1'), ('2', '2'), ('3', '3')]
